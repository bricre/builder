A builder image for packaging software written in PHP and NodeJS, with some basic tools for simple debugging

Tools installed:

- PHP 8.1
- NodeJS 18.0

- Composer
- Git
- SSH
- procps (For top & ps commands)
- vim
- curl
- wget
- zip

update by 2022-06-13